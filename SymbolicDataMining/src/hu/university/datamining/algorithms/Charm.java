/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.university.datamining.algorithms;

import hu.university.datamining.algorithms.Charm.MyHashMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.logging.Logger;

/**
 *
 * @author nor
 */
public class Charm /*implements Algorithm */{

    private static Logger logger = Logger.getAnonymousLogger();

    private int minSupport;
    private MyHashMap hashItNodes;
    int numberOfExtractedItems;

    Map<String, Integer> result = new HashMap<>();
/*
    @Override
    public LinkedList<Node>í execute(String fileName, String columnDelimiter, String dataDelimiter, int minSupport) {
        this.charm(fileName, columnDelimiter, dataDelimiter, minSupport);
        return result;
    }
*/
    private static class ListOfNodes {

        private static class ClosedItNode {

            LinkedList<String> name = new LinkedList<String>();
            int support;
            ClosedItNode next;

            ClosedItNode(LinkedList<String> name, int support) {
                this.name = name;
                this.support = support;
                this.next = null;
            }
        }

        private ClosedItNode head;
        private int size = 0;

        void addNode(ItNode node) {
            if (size == 0) {
                head = new ClosedItNode(node.name, node.supportCount);
                size++;
                return;
            }
            ClosedItNode current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = new ClosedItNode(node.name, node.supportCount);
            size++;
        }

        boolean contains(ItNode node) {
            if (head == null) {
                return false;
            }
            ClosedItNode current = head;
            while (current != null) {
                if (current.support == node.supportCount) {
                    //TODO Hibalehet�s�g
                    Collections.sort(current.name);
                    Collections.sort(node.name);
                    if (current.name.equals(node.name) || containsItemsets(current.name, node.name)) {
                        return true;
                    }
                }
                current = current.next;
            }
            return false;
        }
    }

    static boolean containsItemsets(LinkedList<String> name, LinkedList<String> name2) {
        int itContains = 0;
        for (String n2 : name2) {
            for (String n : name) {
                if (n2.equals(n)) {
                    itContains++;
                }
            }
        }
        if (itContains >= name2.size()) {
            return true;
        }
        return false;
    }

    class MyHashMap {

        int sor;
        ListOfNodes[] list;

        MyHashMap(int sor) {
            this.sor = sor;
            this.list = new ListOfNodes[sor + 1];
        }

        boolean addElement(ItNode itnode) {
            int igazak = 0;
            int tidsetOsszeg = 0;
            for (int i = 0; i < itnode.ba.size(); i++) {
                if (itnode.ba.get(i)) {
                    tidsetOsszeg += i;
                    igazak++;
                }
            }
            int kulcs = tidsetOsszeg % sor;

            if (list[kulcs] == null) {
                list[kulcs] = new ListOfNodes();
                list[kulcs].addNode(itnode);
                return true;
            } else if (!list[kulcs].contains(itnode)) {
                list[kulcs].addNode(itnode);
                return true;
            }
            return false;
        }
    }

    static void supportCount(ItNode itnode) {
        int mySupport = 0;
        for (int i = 0; i < itnode.ba.size(); i++) {
            if (itnode.ba.get(i).equals(true)) {
                mySupport++;
            }
        }
        itnode.supportCount = mySupport;
    }

    static void replaceInSubTree(ItNode curr, ItNode other) {
        ItNode newItNode = new ItNode();
        for (String s : curr.name) {
            newItNode.name.add(s);
        }
        for (int l = 0; l < other.name.size(); l++) {
            boolean benneVanE = false;;
            for (int k = 0; k < newItNode.name.size(); k++) {
                if (newItNode.name.get(k).equals(other.name.get(l))) {
                    benneVanE = true;
                    break;
                }
            }
            if (!benneVanE) {
                newItNode.name.add(other.name.get(l));
                break;
            }
        }

        curr.name = new LinkedList<String>();
        for (String s : newItNode.name) {
            curr.name.add(s);
        }
        for (ItNode itnode : curr.children) {
            replaceInSubTree(itnode, other);
        }

    }

    void deleteItNode(ItNode itnode, LinkedList<ItNode> itnodes) {
        int index = 0;
        //Original D code foreach(i,node;itnodes)
        for (int i = 0; i < itnodes.size(); i++) {
            if (itnodes.get(i).name.equals(itnode.name)) {
                index = i;
                break;
            }
        }
        itnodes.remove(index);
    }

    static void supportCount(LinkedList<ItNode> itnodes) {
        for (ItNode itnode : itnodes) {
            int mySupport = 0;
            for (int i = 0; i < itnode.ba.size(); i++) {
                if (itnode.ba.get(i).equals(true)) {
                    mySupport++;
                }
            }
            itnode.supportCount = mySupport;
        }
    }

    void save(ItNode curr) {
        if (hashItNodes.addElement(curr)) {
          //  System.out.println(curr.name + " " + curr.supportCount + ";" + curr.ba);
            result.put(curr.name.toString(), curr.supportCount);
           // System.out.println(curr.name);
            numberOfExtractedItems++;
        }
    }

    Charm() {
    }

    void extend(ItNode curr) {
        for (int i = 0; i < curr.parent.children.size(); i++) {
            ItNode other = curr.parent.children.get(i);
            if (containsItemsets(curr.name, other.name)) {
                continue;
            }
            if (curr.ba.equals(other.ba)) {
                replaceInSubTree(curr, other);
                deleteItNode(other, curr.parent.children);
                i--;
            } else if (isSubList(curr.ba, other.ba)) {
                replaceInSubTree(curr, other);
            } else if (isSubList(other.ba, curr.ba)) {
                ItNode candidate = getCandidate(curr, other);
                deleteItNode(other, curr.parent.children);
                i--;
                //	writeln("curr.isSubset");
                /*A nagy N jelenti ,hogy Nem lett megfelelo a jelolt a gyerekek koze valo felvetelhez*/
                if (!candidate.name.get(0).equals("N")) {
                    curr.addChild(candidate);
                }
            } else {
                ItNode candidate = getCandidate(curr, other);
                if (!candidate.name.get(0).equals("N")) {
                    curr.addChild(candidate);
                }
            }
        }

        while (curr.children.size() > 0) {
            ItNode child = curr.getFirstChild();
            if (child.name.get(0).equals("N")) {
                break;
            }
            extend(child);
            save(child);
            //       delete(child);
        }
    }

    public boolean isSubList(LinkedList<Boolean> list1, LinkedList<Boolean> list2) {
        int igazak = 0;
        for (int i = 0; i < list1.size(); i++) {
            if (list1.get(i)) {
                igazak++;
            }
        }
        int talalt = 0;
        for (int i = 0; i < list2.size(); i++) {
            if (list1.get(i) && list2.get(i)) {
                talalt++;
            }
        }
        if (talalt == igazak) {
            return true;
        }
        return false;
    }

    private ItNode getCandidate(ItNode curr, ItNode other) {
        ItNode newItNode = new ItNode();
        for (String s : curr.name) {
            newItNode.name.add(s);
        }
        //newItNode.name=curr.name.dup;
        for (int l = 0; l < other.name.size(); l++) {
            boolean benneVanE = false;
            for (int k = 0; k < newItNode.name.size(); k++) {
                if (newItNode.name.get(k).equals(other.name.get(l))) {
                    benneVanE = true;
                    break;
                }
            }
            if (!benneVanE) {
                /*newItNode.name.length=newItNode.name.length+1;
                 newItNode.name[newItNode.name.length-1]=other.name[l];
                 newItNode.ba=curr.ba & other.ba;*/
                newItNode.name.add(other.name.get(l));
                newItNode.ba = new LinkedList<Boolean>();
                for (int i = 0; i < curr.ba.size(); i++) {
                    if (curr.ba.get(i).equals(other.ba.get(i)) && curr.ba.get(i).equals(true) && other.ba.get(i).equals(true)) {
                        newItNode.ba.add(true);
                    } else {
                        newItNode.ba.add(false);
                    }
                }
                break;
            }
        }
        supportCount(newItNode);
        if (newItNode.supportCount >= minSupport) {
            return newItNode;
        } else {
            newItNode.name.set(0, "N");
            return newItNode;
        }
    }

    // void charm(LinkedList<ItNode> itnodes)
    public void charm(String fileName, String columnDelimiter, String dataDelimiter, int minSupport) {
        this.minSupport = minSupport;
        LinkedList<ItNode> itnodes = new LinkedList<>();
        BufferedReader br = null;
        try {
            File f = new File(fileName);
            br = new BufferedReader(new FileReader(f));

            String sCurrentLine = br.readLine();
            StringTokenizer st = new StringTokenizer(sCurrentLine, columnDelimiter);
            while (st.hasMoreElements()) {
                ItNode in = new ItNode();
                in.name.add(st.nextToken());
                itnodes.add(in);
            }
            int sorok = 0;
            while ((sCurrentLine = br.readLine()) != null) {
                StringTokenizer st2 = new StringTokenizer(sCurrentLine, dataDelimiter);
                sorok++;
                for (ItNode in : itnodes) {
                    int szam = Integer.valueOf(st2.nextToken()).intValue();
                    if (szam == 1) {
                        in.ba.add(true);
                    } else {
                        in.ba.add(false);
                    }
                }
            }
            this.hashItNodes = new MyHashMap(sorok);
            ItNode root = new ItNode();
            supportCount(itnodes);
            for (ItNode itnode : itnodes) {
                if (itnode.supportCount >= minSupport) {
                    root.addChild(itnode);
                }
            }

            while (root.children.size() > 0) {
//		writeln(root.children);
                ItNode child = root.getFirstChild();
                if (child.name.get(0).equals("N")) {
                    break;
                }
                extend(child);
                save(child);
                // delete(child);
                itnodes.remove(child);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
