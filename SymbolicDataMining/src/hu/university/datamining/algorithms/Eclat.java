/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.university.datamining.algorithms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import hu.university.datamining.algorithms.Node;

public class Eclat extends Apriori implements Algorithm{

	//private Map<String,Integer> result=new HashMap<String,Integer>();
    private LinkedList<Node> result=new LinkedList<Node>();

    @Override
    public LinkedList<Node> execute(String fileName, String columnDelimiter, String dataDelimiter, int minSupport) {
       this.fileName=fileName;
       this.minSupport=minSupport;
       this.miner(columnDelimiter, dataDelimiter);
       return result;
    }
	
	protected class ItNode extends Node{
		LinkedList<ItNode> children=new LinkedList<ItNode>();
		ItNode parent;

		void addChild(ItNode attr)
		{
		        attr.parent=this;
		        this.children.add(attr);
		}

		ItNode getFirstChild()
		{
		        if(children.size()>0)
		        {
		        ItNode first=children.peekFirst();
		        children.remove();
		        return first;
		        }
		        else
		        {
		        return null;
		        }
		}
		}
	
	protected void count()
	{
		int support;
		for(ItNode it : itNodes)
		{
			support=0;
			Vector<Boolean> v=it.ba;
//			System.out.println(v.capacity());
			for(Boolean b : v)
			{
				if(b.booleanValue()==true)
					support++;
			}
			it.supportCount=support;
		}
	}
	
	Eclat()
	{}

	protected LinkedList<ItNode> itNodes = new LinkedList<ItNode>();
	
	@Override
	public void miner(String columnDelimiter,String dataDelimiter)
	{
		BufferedReader br=null;
		try{
		//	URL l = Apriori.class.getResource(this.fileName);
			File f = new File(fileName);
			br=new BufferedReader(new FileReader(f));

			String sCurrentLine=br.readLine();
			StringTokenizer st=new StringTokenizer(sCurrentLine,columnDelimiter);
			while(st.hasMoreElements())
			{
				ItNode in=new ItNode();
				in.name.add(st.nextToken());
				itNodes.add(in);
			}
			while((sCurrentLine=br.readLine())!=null)
			{
				StringTokenizer st2=new StringTokenizer(sCurrentLine,dataDelimiter);
			//	System.out.println(st2.countTokens());
				for(ItNode in : this.itNodes)
				{
					int szam=Integer.valueOf(st2.nextToken()).intValue();
					if(szam==1)
						in.ba.add(true);
					else
						in.ba.add(false);
				}
			}
			this.count();
			this.eclat();
		}catch(IOException io)
		{
			System.out.println(io.getMessage());
		}
		finally
		{
			try{
			if(br!=null)
			br.close();
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
		}
	}
	
	protected ItNode supportCount(ItNode in)
	{
		int support=0;
		for(Boolean b : in.ba)
		{
			if(b.booleanValue()==true)
				support++;
		}
		in.supportCount=support;
		return in;
	}
	
	protected ItNode getCandidate(ItNode curr, ItNode other)
	{
		ItNode newItNode = new ItNode();
		newItNode.name.addAll(curr.name);
		for(int i=0;i<curr.name.size();i++)
		{
			if(!curr.name.contains(other.name.get(i)))
			{
				newItNode.name.add(other.name.get(i));
			}
		}
		for(int i=0;i<curr.ba.size();i++)
		{
			if(curr.ba.get(i).booleanValue()==other.ba.get(i) && other.ba.get(i)==true)
			{
				newItNode.ba.add(true);
			}
			else
				newItNode.ba.add(false);
		}
		newItNode=supportCount(newItNode);
		if(newItNode.supportCount>=minSupport)
		{
		return newItNode;
		}
		else
			return null;
	}
	
	protected void extend(ItNode curr)
	{
		for(int i=0;i<curr.parent.children.size();i++)
		{
			ItNode candidate=getCandidate(curr,curr.parent.children.get(i));
//			System.out.println(curr.name+";"+curr.parent.children.get(i).name);
			if(candidate!=null)
			{
				curr.addChild(candidate);
			}
		}
		
		while(!curr.children.isEmpty())
		{
			ItNode child=curr.getFirstChild();
			if(child==null)
				break;
			extend(child);
			save(child);
//			delete(child);
		}
	}
	
	protected void save(ItNode in)
	{
	//	System.out.println(in);
		/*StringBuilder sb=new StringBuilder();
		for(String s: in.name)
		{
			sb.append(s);
		}*/
		this.result.add(in);
	//	System.out.println(result.containsKey(sb.toString()));
	}
	
	public LinkedList<Node> getResult() {
		return result;
	}
	
	protected void eclat()
	{
		ItNode root = new ItNode();
		root.name.add("root");
		this.count();
		for(ItNode itnode : itNodes)
		{
			if(itnode.supportCount>=minSupport)
			{
				root.addChild(itnode);
			}
		}
		while(!root.children.isEmpty())
		{
			 ItNode child=root.getFirstChild();
             if(child==null)
             break;
             extend(child);
             save(child);
            // delete(child);
		}
	}

}

