/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.university.datamining.algorithms;

import java.io.FileReader;
import java.io.File;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;
import java.util.logging.Logger;
import hu.university.datamining.algorithms.Node;

/**
 *
 * @author nor
 */
public class Apriori implements Algorithm {

    private static Logger logger = Logger.getAnonymousLogger();
    /**
     * The result of the symbolic datamining
     */
    private int runCount = 0;
    //  private Map<String, Integer> result = new TreeMap<String, Integer>();
    LinkedList<Node> result = new LinkedList<Node>();

    @Override
    public LinkedList<Node> execute(String fileName, String columnDelimiter, String dataDelimiter, int minSupport) {
        this.fileName = fileName;
        this.minSupport = minSupport;
        this.miner(columnDelimiter, dataDelimiter);
        return result;
    }

    Apriori() {
    }

    protected void count() {
        int support;
        for (Node it : itNodes) {
            support = 0;
            Vector<Boolean> v = it.ba;
            for (Boolean b : v) {
                if (b.booleanValue() == true) {
                    support++;
                }
            }
            it.supportCount = support;
        }
    }

    protected String fileName;
    private LinkedList<Node> itNodes = new LinkedList<Node>();
    protected int minSupport;

    private int getSupport(int index) {
        return itNodes.get(index).supportCount;
    }

    public Apriori(String fileName, int minSupport) {
        this.fileName = fileName;
        this.minSupport = minSupport;
    }

    private LinkedList<Node> gen(LinkedList<Node> frequentNodes) {
        LinkedList<Node> newNodes = new LinkedList<>();
        for (int i = 0; i < frequentNodes.size(); i++) {
            for (int j = i + 1; j < frequentNodes.size(); j++) {
                Node first = frequentNodes.get(i);
                Node second = frequentNodes.get(j);
                Vector<Boolean> firstBa = first.ba;
                Vector<Boolean> secondBa = second.ba;
                int egyezes = 0;
                Node found = new Node();
                for (int g = 0; g < first.ba.size(); g++) {
                    if (firstBa.get(g).equals(secondBa.get(g)) && firstBa.get(g) == true) {
                        egyezes++;
                        found.ba.add(true);
                    } else {
                        found.ba.add(false);
                    }
                }
                for (String s : first.name) {
                    if (!found.name.contains(s)) {
                        found.name.add(s);
                    }
                }
                for (String s : second.name) {
                    if (!found.name.contains(s)) {
                        found.name.add(s);
                    }
                }
                found.supportCount = egyezes;
                if (egyezes >= minSupport) {
                      boolean talalt = false;
                    long szam = 0;
                    if (result.size() > 1) {
                      /* for (Node it : result) {
                            szam = 0;*/
                            /*for (String s : it.name) {
                                if (found.name.contains(s)) {
                                    szam++;
                                }
                            }*/
                            if(result.contains(found) || newNodes.contains(found))
                                talalt=true;
                    }
                    if (talalt) {
                        break;
                    } else {
                        newNodes.add(found);
                    }
                }
            }
        }
        runCount++;
        System.out.print(runCount + " ");
        newNodes.stream().forEach(System.out::println);
        return newNodes;
    }

    public void miner(String columnDelimiter, String dataDelimiter) {
        BufferedReader br = null;
        try {
            File f = new File(this.fileName);
            br = new BufferedReader(new FileReader(f));

            String sCurrentLine = br.readLine();
            StringTokenizer st = new StringTokenizer(sCurrentLine, columnDelimiter);
            while (st.hasMoreElements()) {
                Node in = new Node();
                in.name.add(st.nextToken());
                itNodes.add(in);
            }
            while ((sCurrentLine = br.readLine()) != null) {
                StringTokenizer st2 = new StringTokenizer(sCurrentLine, dataDelimiter);
                for (Node in : this.itNodes) {
                    int szam = Integer.valueOf(st2.nextToken()).intValue();
                    if (szam == 1) {
                        in.ba.add(true);
                    } else {
                        in.ba.add(false);
                    }
                }
            }
            this.count();
            while (itNodes.size() > 0) {
                this.count();
                LinkedList<Node> frequent = new LinkedList<Node>();
                for (int j = 0; j < itNodes.size(); j++) {
                    if (itNodes.get(j).supportCount >= minSupport) {
                        frequent.add(itNodes.get(j));
                    }
                }
                int l = 0;/*
                for (Node in : frequent) {
                    StringBuilder sb = new StringBuilder();
                    for (String s : in.name) {
                        sb.append(s);
                    }
                    result.add(sb.toString(), in.supportCount);
                }*/
                result.addAll(frequent);
                itNodes = gen(frequent);
            }

        } catch (IOException io) {
            System.out.println(io.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public LinkedList<Node> getResult() {
        return result;
    }
}
