/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.university.datamining.algorithms;

import hu.university.datamining.algorithms.Node;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public interface Algorithm {
	
    /**
     *
     * @param fileName Data file name.
     * @param columnDelimiter
     * @param dataDelimiter
     * @return key value pairs. Key is the frequent itemset. Value is the
     * support count.
     */
    public LinkedList<Node> execute(String fileName, String columnDelimiter, String dataDelimiter, int minSupport);

    public static Algorithm getApriori() {
        return new Apriori();
    }

    public static Algorithm getEclat() {
        return new Eclat();
    }
/*
    public static Algorithm getCharm() {
        return new Charm();
    }*/
}
