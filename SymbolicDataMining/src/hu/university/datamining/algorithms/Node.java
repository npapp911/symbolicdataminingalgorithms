/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.university.datamining.algorithms;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Vector;

/**
 *
 * @author nor
 */
public class Node {

        LinkedList<String> name = new LinkedList<String>();
        int supportCount;
        Vector<Boolean> ba = new Vector<Boolean>();

        Node(LinkedList<String> name, int supportCount, Vector<Boolean> ba) {
            this.name = name;
            this.supportCount = supportCount;
            this.ba = ba;
        }

        Node() {
        }

        @Override
        public boolean equals(Object o){
            if(o==null)
                return false;
            if(!(o instanceof Node))
                return false;
            Node n=(Node)o;
            //System.out.println(n.name+" "+this.name);
            Collections.sort(n.name);
            Collections.sort(this.name);
            if(!(n.name.equals(this.name)))
                return false;
            return true;        
        }
        
        

        LinkedList<String> getName() {
            return name;
        }

        public String toString() {
            return "Name:" + this.name + " SupportCount:" + this.supportCount;
        }
    }
