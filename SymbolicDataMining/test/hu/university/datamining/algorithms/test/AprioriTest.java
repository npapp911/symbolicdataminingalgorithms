/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.university.datamining.algorithms.test;

import hu.university.datamining.algorithms.Algorithm;
import hu.university.datamining.algorithms.Apriori;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nor
 */
public class AprioriTest {
    private static Algorithm apriori;
    private Map<String,Integer> resultList;
    private Map<String,Integer> resultList2;
    private Map<String,Integer> resultList3;
    
    public AprioriTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {    
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        resultList=new HashMap<>();
        resultList.put("A",4);
        resultList.put("B",3);
        resultList.put("C",3);
        resultList.put("E",3);
        resultList.put("F",4);
        resultList.put("AC",3);
        resultList.put("AE",3);
        resultList.put("AF",3);
        resultList.put("BF",3);
        resultList.put("CF",3);
        resultList.put("ACF",3);
        
        resultList2=new HashMap<>();
        resultList2.put("A",4);
        resultList2.put("B",5);
        resultList2.put("E",5);
        resultList2.put("F",4);
        resultList2.put("BF",4);
        resultList2.put("BE",4);
        resultList2.put("EF",4);
        resultList2.put("BEF",4);
        
       resultList3=new HashMap<>();
       resultList3.put("Apple",3);
       resultList3.put("Orange",4);
       resultList3.put("Peach",4);
       resultList3.put("OrangePeach",3);
    }
    
    @After
    public void tearDown() {
    }


     @Test
     public void test() {
      /*   Map result = Algorithm.getApriori().execute("test.txt", ",", ",", 3);
         assertEquals(resultList,result);
         for(Object s : result.keySet()){
             System.out.println((String)s);
         }
 
         Map result2 = Algorithm.getApriori().execute("test2.txt", "|", ",", 4);
         assertEquals(resultList2,result2);

         Map result3 = Algorithm.getApriori().execute("test3.txt", "|", ",", 3);
         assertEquals(resultList3,result3);*/
     }
}
